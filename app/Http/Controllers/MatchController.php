<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Models\Match;
use App\TicTacToeGame;
use App\Judge;

class MatchController extends Controller {

    protected $ticTacToeGame = null;

    public function __construct()
    {
        $this->ticTacToeGame = new TicTacToeGame();
    }

    public function index() {
        return view('index');
    }

    /**
     * Returns a list of matches
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function matches() {
        return response()->json($this->ticTacToeGame->matches());
    }

    /**
     * Returns the state of a single match
     *
     * TODO it's mocked, make this work :)
     *
     * @param $id Match's Id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function match($id)
    {
       return response()->json($this->ticTacToeGame->match((int) $id));
    }

    /**
     * Makes a move in a match
     *
     * TODO it's mocked, make this work :)
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function move($id) {
        return response()->json($this->ticTacToeGame->playerMove((int) $id, Input::get('position')));
    }

    /**
     * Creates a new match and returns the new list of matches
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create() {
        return response()->json($this->ticTacToeGame->newMatch());
        
    }

    /**
     * Deletes the match and returns the new list of matches
     *
     * @param $id
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        return response()->json($this->ticTacToeGame->remove((int) $id));
    }

    /**
     * Creates a fake array of matches
     *
     * @return \Illuminate\Support\Collection
     */
    private function fakeMatches() {
        return collect([
            [
                'id' => 1,
                'name' => 'Match1',
                'next' => 2,
                'winner' => 1,
                'board' => [
                    1, 0, 2,
                    0, 1, 2,
                    0, 2, 1,
                ],
            ],
            [
                'id' => 2,
                'name' => 'Match2',
                'next' => 1,
                'winner' => 0,
                'board' => [
                    1, 0, 2,
                    0, 1, 2,
                    0, 0, 0,
                ],
            ],
            [
                'id' => 3,
                'name' => 'Match3',
                'next' => 1,
                'winner' => 0,
                'board' => [
                    1, 0, 2,
                    0, 1, 2,
                    0, 2, 0,
                ],
            ],
            [
                'id' => 4,
                'name' => 'Match4',
                'next' => 2,
                'winner' => 0,
                'board' => [
                    0, 0, 0,
                    0, 0, 0,
                    0, 0, 0,
                ],
            ],
        ]);
    }


}