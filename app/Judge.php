<?php
namespace App;

/**
 * Represents to the game's Judge, determines who is the winner
 *
 * @package App.Judge
 * @author  Roberto Bargiela <roberto.bargiela@gmail.com>
 * @link    https://bitbucket.org/rbargiela/php-fullstack-test/src/master/
 */
class Judge
{
    protected static $winningLines = [
            // horizontal
            'h1' => [0,1,2],
            'h2' => [3,4,5],
            'h3' => [6,7,8],
            
            // vertical
            'v1' => [0,3,6],
            'v2' => [1,4,7],
            'v3' => [2,5,8],
            
            //diagonal
            'd1' => [0,4,8],
            'd2' => [2,4,6]
        ];

    /**
     * Analyze the game and verify if there is winner
     * 
     * @param array $board     The current match
     * @param int   $playerOne Number that represents the player one
     * @param int   $playerTwo Number that represents the player two
     * 
     * @return int Returns zero if there is not a winner yet, or the number of the winner player otherwise
     */
    public static function whoIsTheWinner(array $board, int $playerOne, int $playerTwo)
    {
        $movesPlayer1 = self::playersMove($board, $playerOne);
        $movesPlayer2 = self::playersMove($board, $playerTwo);

        $winnerIs = 0;
        foreach (self::$winningLines as $aWinningLine) {
            if (self::hasWon($aWinningLine, $movesPlayer1)) {
                $winnerIs = $playerOne; break;
            } else if (self::hasWon($aWinningLine, $movesPlayer2)) {
                $winnerIs = $playerTwo; break;
            }
        }
        return $winnerIs;
    }

    /**
     * Check if the player's move is a winning line
     * 
     * @param array $winningLine One of the winning lines, represented by the board's positions
     * @param array $playerMove  The player's move
     * 
     * @return boolean True if has won, false in other case
     */
    public static function hasWon(array $winningLine, array $playerMove)
    {
        return (count(array_intersect($winningLine, $playerMove))==3);
    }

    /**
     * Extracts from the match board all the moves for a player
     * 
     * @param array $board        Match board
     * @param int   $playerNumber Number of the player
     * 
     * @return array
     */
    private static function playersMove(array $board, int $playerNumber)
    {
        return array_keys($board, $playerNumber);
    }
}