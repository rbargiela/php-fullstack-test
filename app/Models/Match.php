<?php
/**
 * ORM definition for Match
 *
 * @package TicTacToe.Model.Match
 * @author  Roberto Bargiela <roberto.bargiela@gmail.com>
 * @link    https://bitbucket.org/rbargiela/php-fullstack-test/src/master/
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

/**
 * Represent a Match object
 *
 * @package TicTackToe.Model.Match
 * @author  Roberto Bargiela <roberto.bargiela@gmail.com>
 * @link    https://bitbucket.org/rbargiela/php-fullstack-test/src/master/
 */
class Match extends EloquentModel
{

    protected $table = "matches";

    protected $fillable = ['name', 'next', 'winner', 'board'];
}