<?php
namespace App;

use App\Models\Match;

class TicTacToeGame
{

    public function matches()
    {
        return Match::all();
    }

    public function match(int $id)
    {
        $match = Match::find($id);

        $rMatch = [
            'id' => $match->id,
            'next' => $match->next,
            'winner' => $match->winner,
            'board' => json_decode($match->board, true)
        ];

        return $rMatch;
    }

    public function newMatch()
    {
        $lastMatchId = (Match::max('id')??0);
        $match = Match::create(
            [
                'name' => 'Match ' . (++$lastMatchId),
                'next' => 2,
                'winner' => 0,
                'board' => json_encode(
                    [
                        0, 0, 0,
                        0, 0, 0,
                        0, 0, 0
                    ]
                )
            ]
        );
        return $match->get();
    }

    public function remove(int $id)
    {
        $match = Match::find($id);
        if ($match) {
            $match->delete();
        }
        return Match::all();
    }

    public function playerMove(int $id, int $position)
    {
        $match = Match::find($id);

        $board = json_decode($match->board, true);

        $board[$position] = $match->next;
        $match->board = json_encode($board);
        $match->next = ($match->next==1?2:1);
        $match->winner = Judge::whoIsTheWinner($board, 1, 2);
        $match->save();

        $rMatch = [
            'id' => $match->id,
            'next' => $match->next,
            'winner' => $match->winner,
            'board' => json_decode($match->board, true)
        ];

        return $rMatch;
    }

}