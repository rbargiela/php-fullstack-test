<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Judge;

class JudgeTest extends TestCase
{
    public function testHasWon()
    {
        $this->assertEquals(false, Judge::hasWon([0,4,8], [1,2,3]), 'Expecting no winner, but apparently there is a winner');
        $this->assertEquals(true, Judge::hasWon([0,4,8], [0,4,8]), 'Expecting a winner, but apparently there is not.');
    }

    public function testPlayersMove()
    {
        $playerOne = 1;
        $playerTwo = 2;
        $boardMatch1 = [
            1,2,2,
            2,1,1,
            0,1,2
        ];

        $this->assertEquals(0, Judge::whoIsTheWinner($boardMatch1, $playerOne, $playerTwo));

        $boardMatch2 = [
            1,0,2,
            1,2,0,
            2,0,0
        ];
        $this->assertEquals(2, Judge::whoIsTheWinner($boardMatch2, $playerOne, $playerTwo));

        $boardMatch3 = [
            0,2,1,
            0,2,2,
            1,1,1
        ];
        $this->assertEquals(1, Judge::whoIsTheWinner($boardMatch3, $playerOne, $playerTwo));
    }
}