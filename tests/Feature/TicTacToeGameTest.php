<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \Database\Factories\MatchFactory;
use \App\TicTacToeGame;
use \App\Models\Match;


class TicTacToeGameTest extends TestCase
{
    use RefreshDatabase;

    protected $ticTacToeGame;

    public function setUp()
    {
        $this->ticTacToeGame = new TicTacToeGame();
    }

    public function testListAllMatches()
    {
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
        $emptyList = [];
        $this->assertEquals($emptyList, $this->ticTacToeGame->matches());

        $fourMatches = [
            [
                'id' => 1,
                'name' => 'Match1',
                'next' => 2,
                'winner' => 1,
                'board' => [
                    1, 0, 2,
                    0, 1, 2,
                    0, 2, 1,
                ],
            ],
            [
                'id' => 2,
                'name' => 'Match2',
                'next' => 1,
                'winner' => 0,
                'board' => [
                    1, 0, 2,
                    0, 1, 2,
                    0, 0, 0,
                ],
            ],
            [
                'id' => 3,
                'name' => 'Match3',
                'next' => 1,
                'winner' => 0,
                'board' => [
                    1, 0, 2,
                    0, 1, 2,
                    0, 2, 0,
                ],
            ],
            [
                'id' => 4,
                'name' => 'Match4',
                'next' => 2,
                'winner' => 0,
                'board' => [
                    0, 0, 0,
                    0, 0, 0,
                    0, 0, 0,
                ],
            ],
        ];
        $this->assertEquals($fourMatches, $this->ticTacToeGame->matches());

    }

    public function testNewMatch()
    {
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
        $match = [
                'id' => 1,
                'name' => 'Match1',
                'next' => 2,
                'winner' => 1,
                'board' => [
                    1, 0, 2,
                    0, 1, 2,
                    0, 2, 1,
                ],
            ];
        $this->assertEquals($match, $this->ticTacToeGame->newMatch());
    }

    public function testJoinMatch()
    {
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
        $matchId = 2;
        $this->assertEquals($match, $this->ticTacToeGame->match($matchId));
    }

    public function testRemoveMatch()
    {
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
        $matchId = 1;
        $this->assertEquals($match, $this->ticTacToeGame->remove($matchId));
    }

    public function testPlayerMove()
    {
        $this->markTestIncomplete(
          'This test has not been implemented yet.'
        );
        $matchId = 1;
        $boardPosition = 3;
        $match = Match::find(1);
        $this->assertEquals($match, $this->ticTacToeGame->playerMove($matchId, $boardPosition));
    }
}