<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try{
            Schema::create(
                'matches', 
                function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('name');
                    $table->integer('next')->default(0);
                    $table->integer('winner')->default(0);
                    $table->json('board');
                    $table->datetime('created_at')->default(null);
                    $table->datetime('updated_at')->default(null);
                }
            );
        } catch(QueryException $e) {
            $this->down();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
