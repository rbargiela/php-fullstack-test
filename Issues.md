# Issues

To solve some issues during docker build I made this changes in app.dockerfile:

* mysql-client is no longer used. I use mariadb-client intead.
* Run npm install fails. So I changed this https://deb.nodesource.com/setup_6.x for https://deb.nodesource.com/setup_10.x

After that, I apply some permissions:
* Permissions to write in bootstrap/cache and logs

Later one change more because there is a mismatch version conflic with composer:
* Composer mismatch version. PackageManigest undefined index 'name' in line 120.

